const itemHidden = document.querySelectorAll(".hidden-item");
const btnShow = document.querySelector("#load-more");

btnShow.addEventListener("click", onShowItem);

function onShowItem(e) {
  let textBtn = btnShow.textContent;

  itemHidden.forEach((item) => {
    item.classList.toggle("visible");
  });

  if (textBtn === "load more") {
    btnShow.textContent = "close";
  }
  if (textBtn === "close") {
    btnShow.textContent = "load more";
  }
}
