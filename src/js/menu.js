const wrapperMenuEl = document.querySelector(".overflow-nav-mob");
const burgerWrapper = document.querySelector(".burger");
const decorationBurgerEl = document.querySelector("#decoration-burger");
const listEl = document.querySelector(".list-nav");

burgerWrapper.addEventListener("click", onOpenMenu);
listEl.addEventListener("click", onClose);

function onOpenMenu() {
  if (decorationBurgerEl.classList.value === "dash-close") {
    decorationBurgerEl.classList.remove("dash-close");
    decorationBurgerEl.classList.add("dash-open");
  } else {
    decorationBurgerEl.classList.remove("dash-open");
    decorationBurgerEl.classList.add("dash-close");
  }

  wrapperMenuEl.classList.toggle("open-menu");
}
function onClose(e) {
  if (e.target.nodeName !== "A") {
    return;
  }
  onOpenMenu();
}
