const listEl = document.querySelector(".list-card-firmware");
const itemEl = document.querySelectorAll(".item-firmware");
const showBtn = document.querySelectorAll(".show-btn");

itemEl.forEach((el) => {
  const text = el.querySelector(".paragraph-card-text");

  el.addEventListener("click", (e) => {
    const t = e.target;

    if (t.classList.contains("show-btn")) {
      text.classList.toggle("close-text");
    }
  });
});

showBtn.forEach((btn) => {
  const text = btn.querySelector(".showBtn");

  btn.addEventListener("click", (e) => {
    const b = e.target;

    const textBtn = b.textContent;
    if (textBtn === "show more") {
      b.textContent = "less more";
    } else {
      b.textContent = "show more";
    }
  });
});
