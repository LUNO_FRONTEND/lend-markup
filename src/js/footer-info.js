const listEl = document.querySelector("#footer-info-up");
const boxFirmwareEl = document.querySelector("#addEventFirm");
const boxDevicesEl = document.querySelector("#addEventDevic");
const firmwareEl = document.querySelector("#current-firmware");
const devicesEl = document.querySelector("#current-devices");

boxFirmwareEl.addEventListener("click", onOpenInfoFirmware);
boxDevicesEl.addEventListener("click", onOpenInfoDevices);

function onOpenInfoFirmware(e) {
  if (
    e.target.nodeName !== "DIV" &&
    e.target.nodeName !== "P" &&
    e.target.nodeName !== "svg"
  ) {
    return;
  }

  firmwareEl.classList.toggle("open-info");
  console.log(e.target.nodeName);
}
function onOpenInfoDevices(e) {
  if (
    e.target.nodeName !== "DIV" &&
    e.target.nodeName !== "P" &&
    e.target.nodeName !== "svg"
  ) {
    return;
  }

  devicesEl.classList.toggle("open-info");
  console.log(e.target.nodeName);
}
